const { existsSync, readFileSync } = require('fs');
const { execSync } = require('child_process');

// use a node library.  not required but easier for this example
const jwt = require('jsonwebtoken');

const NAME = 'example';

// token configuration
const subject = 'xxxx';  // subject value provided by Prime
const audience = 'xxxx'; // consumer key for the environment provided by Prime
const expiresIn = 3600   // cannot be more than 1 hour (3600s)
const issuer = `http://${NAME}.com`; // must be sent to prime
const algorithm = 'RS256';

const publicKeyFilename = `${NAME}.crt`;
const privateKeyFilename = `${NAME}.key`;

// create public/private keys
if (!existsSync(publicKeyFilename) || !existsSync(privateKeyFilename)) {
  const SUBJECT = {
    'C': 'US',            // country code
    'ST': 'NY',           // state
    'L': 'Buffalo',       // locality
    'O': 'SaveOnSP',      // organization
    'OU': 'IT',           // organizational unit
    'CN': 'saveonsp.com', // common name (fqdn0
  };
  
  const args = [
    'openssl',
    'req',
    '-newkey', 'rsa:4096',
    '-x509',
    '-sha256',
    '-days', 3650, // days (10 years)
    '-nodes',      // no password prompt
    '-out',  publicKeyFilename,
    '-keyout', privateKeyFilename,
    '-subj', `"${Object.entries(SUBJECT).map(([k, v]) => `/${k}=${v}`).join('')}"`,
  ];
  
  const command = args.join(' ');
  execSync(command);
}

const privateKey = readFileSync(privateKeyFilename, 'utf8');
const publicKey = readFileSync(publicKeyFilename, 'utf8');

// test payload
const payload = {};

// sign the payload. token header/payload can be verified at https://jwt.ms/
const token = jwt.sign(payload, privateKey, {issuer, subject, audience, algorithm, expiresIn, mutatePayload: true});
console.log(`token: ${token}\n`);

// view whole token contents
// console.log(jwt.decode(token, {complete: true}));

// this would be run by Prime to verify that the token is legit

console.log(`token contents: ${JSON.stringify(jwt.verify(token, publicKey, {audience, issuer, subject, algorithms: [algorithm], complete: true}), null, 2)}`);
